# Milestone Planner

Milestone Planner allows you to set goals and share them with your friends!
## Installation

Install Maven and run below:
```cmd
mvn clean install
mvn spring-boot:run
```
Then connect to localhost:8080

## Login Credentials

These are the login details for this prototype application:<br />

Username: Bret123<br />
Password: test

Username: Andy235<br />
Password: test