var xhrGetMilestones = new XMLHttpRequest();
xhrGetMilestones.open('GET', "http://localhost:8080/getMilestones", true);
xhrGetMilestones.send();

xhrGetMilestones.addEventListener("readystatechange", function () {
    processMilestoneRequest(xhrGetMilestones, true);
}, false);

var xhrSharedMilestones = new XMLHttpRequest();
xhrSharedMilestones.open('GET', "http://localhost:8080/getSharedMilestones", true);
xhrSharedMilestones.send();

xhrSharedMilestones.addEventListener("readystatechange", function () {
    processMilestoneRequest(xhrSharedMilestones, false);
}, false);

function processMilestoneRequest(xhrRequest, isOwnMilestones) {
    if (xhrRequest.readyState == 4 && xhrRequest.status == 200) {
        var response = JSON.parse(xhrRequest.responseText);
        for (var key in response) {
            generateMilestone(response[key].description, response[key].dueDate,
                response[key].completionDate, response[key].id, isOwnMilestones);
        }
    }
}

function generateMilestone(description, dueDate, completionDate, id, isOwnMilestones) {
    var milestoneCardContainer = document.createElement('div');
    milestoneCardContainer.className = 'col-md-3';
    var milestoneCard = document.createElement('div');
    milestoneCard.className = 'card';

    var header = document.createElement('div');
    header.className = 'card-header font-weight-bold';
    header.textContent = description;
    milestoneCard.appendChild(header);

    var table = document.createElement('table');
    milestoneCardContainer.appendChild(milestoneCard);


    if (isOwnMilestones) {
        milestoneCard.onclick = function () {
            $("#modalMilestoneForm").modal();
            document.getElementById("formMilestoneId").value = id;
            document.getElementById("formDescription").value = description;
            document.getElementById("formDueDate").value = dueDate;
            document.getElementById("formCompletionDate").value = completionDate;
        };

        document.getElementById("ownMilestones").appendChild(milestoneCardContainer);
    } else {
        document.getElementById("sharedMilestones").appendChild(milestoneCardContainer);
    }

    var trDueDate = document.createElement('tr');
    var thDueDate = document.createElement('th');
    thDueDate.className = "col-sm";
    thDueDate.textContent = "Due Date";
    trDueDate.appendChild(thDueDate);
    var thActualDueDate = document.createElement('th');
    thActualDueDate.textContent = dueDate;
    trDueDate.appendChild(thActualDueDate);
    table.appendChild(trDueDate);

    var trCompletionDate = document.createElement('tr');
    var thCompletionDate = document.createElement('th');
    thCompletionDate.className = "col-sm";
    thCompletionDate.textContent = "Completion Date";
    trCompletionDate.appendChild(thCompletionDate);
    var thActualCompletionDate = document.createElement('th');
    thActualCompletionDate.textContent = completionDate;
    trCompletionDate.appendChild(thActualCompletionDate);
    table.appendChild(trCompletionDate);

    milestoneCard.appendChild(table);
}

function toggleMilestoneModal() {
    $("#modalCreateNewMilestoneForm").modal('toggle');
}

function toggleShareMilestone() {
    $("#modalShareMilestoneForm").modal('toggle');

    var xhrGetMilestones = new XMLHttpRequest();
    xhrGetMilestones.open('GET', "http://localhost:8080/getMilestones", true);
    xhrGetMilestones.send();

    xhrGetMilestones.onload = function () {
        if (xhrGetMilestones.readyState == 4 && xhrGetMilestones.status == 200) {
            var response = JSON.parse(xhrGetMilestones.responseText);
            populateShareForm(response);
        }
    };
}

function populateShareForm(response) {
    var milestoneChkBoxList = $('#milestoneCheckboxes')
    milestoneChkBoxList.empty();
    $.each(response, function (i, milestone) {
        console.log(milestone);
        var li = $('<li/>')
            .addClass('ui-menu-item')
            .attr('role', 'menuitem')
            .appendTo(milestoneChkBoxList);

        var aaa = $('<a>')
            .addClass('ui-all')
            .appendTo(li);

        var input = $('<input/>')
            .addClass('ui-all')
            .attr('type', 'checkbox').attr('value',milestone.id)
            .appendTo(aaa);

        var description = $('<span id="description">')
            .text(milestone.description)
            .appendTo(aaa);

        var milestoneId = $('<p id="milestoneId">').text(milestone.id)
            .attr("hidden", true)
            .appendTo(aaa);
    });
}

function shareMilestone() {
    var usernames = document.getElementById("formUsers").value;
    usernames = usernames.split(";");
    console.log(usernames);

    var milestones=[];
    $(':checkbox:checked').each(function(i){
        milestones.push($(this).val().toString());
    });
    console.log(milestones.join());

    var formData = new FormData();
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/shareMilestones");
    formData.append("milestones", milestones);
    formData.append("users", usernames);
    xhr.send(formData);

    $("#modalShareMilestoneForm").modal('toggle');
}

function createMilestone() {
    var description = document.getElementById("formNewDescription").value;
    var dueDate = document.getElementById("formNewDueDate").value;

    var formData = new FormData();
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/generateMilestone");
    formData.append("description", description);
    formData.append("dueDate", dueDate);
    xhr.send(formData);


    xhr.onload = function () {
        console.log(xhr.responseText);
        if (xhr.status == 200) {
            $('#ownMilestones').empty();
            xhrGetMilestones.open('GET', "http://localhost:8080/getMilestones", true);
            xhrGetMilestones.send();
        }
    }
}

function editMilestone() {
    var id = document.getElementById("formMilestoneId").value;
    var description = document.getElementById("formDescription").value;
    var dueDate = document.getElementById("formDueDate").value;
    var completionDate = document.getElementById("formCompletionDate").value;

    var formData = new FormData();
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/editMilestone");
    formData.append("id", id);
    formData.append("description", description);
    formData.append("dueDate", dueDate);
    formData.append("completionDate", completionDate);
    xhr.send(formData);
    $("#modalMilestoneForm").modal('toggle');

    xhr.onload = function () {
        console.log(xhr.responseText);
        if (xhr.status == 200) {
            $('#ownMilestones').empty();
            xhrGetMilestones.open('GET', "http://localhost:8080/getMilestones", true);
            xhrGetMilestones.send();
        }
    }
}

function removeMilestone() {
    var id = document.getElementById("formMilestoneId").value;
    var formData = new FormData();
    var xhr = new XMLHttpRequest();
    var result = confirm("Are you sure you want to delete this Milestone?");
    if (result) {
        xhr.open("DELETE", "/removeMilestone");
        formData.append("milestoneId", id);
        xhr.send(formData);
        $("#modalMilestoneForm").modal('toggle');

        xhr.onload = function () {
            console.log(xhr.responseText);
            if (xhr.status == 200) {
                $('#ownMilestones').empty();
                xhrGetMilestones.open('GET', "http://localhost:8080/getMilestones", true);
                xhrGetMilestones.send();
            }
        }
    }
}