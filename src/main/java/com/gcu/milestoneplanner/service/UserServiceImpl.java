package com.gcu.milestoneplanner.service;


import com.gcu.milestoneplanner.domain.User;
import com.gcu.milestoneplanner.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public class UserServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findById(s);
        if(user.isPresent()) {
            return user.get();
        } else {
            throw new UsernameNotFoundException("Bad Credentials");
        }
    }
}