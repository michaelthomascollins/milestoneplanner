package com.gcu.milestoneplanner.repository;

import com.gcu.milestoneplanner.domain.Milestone;
import org.springframework.data.repository.CrudRepository;

public interface MilestoneRepository extends CrudRepository<Milestone, String> {
}
