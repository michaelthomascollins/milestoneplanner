package com.gcu.milestoneplanner.repository;

import com.gcu.milestoneplanner.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
}