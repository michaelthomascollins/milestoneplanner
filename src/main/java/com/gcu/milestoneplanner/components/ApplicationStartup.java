package com.gcu.milestoneplanner.components;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gcu.milestoneplanner.domain.Milestone;
import com.gcu.milestoneplanner.domain.User;
import com.gcu.milestoneplanner.repository.MilestoneRepository;
import com.gcu.milestoneplanner.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationStartup.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MilestoneRepository milestoneRepository;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        seedData();
    }

    private void seedData() {
        // read json and write to db
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<User>> userTypeReference = new TypeReference<List<User>>(){};
        InputStream userInputStream = TypeReference.class.getResourceAsStream("/json/users.json");
        TypeReference<List<Milestone>> milestoneTypeReference = new TypeReference<List<Milestone>>(){};
        InputStream milestoneInputStream = TypeReference.class.getResourceAsStream("/json/milestones.json");

        try {
            //Save Users
            List<User> users = mapper.readValue(userInputStream,userTypeReference);
            userRepository.saveAll(users);
            logger.info("Users Saved!");

            //Save Milestones
            List<Milestone> milestones = mapper.readValue(milestoneInputStream,milestoneTypeReference);
            milestoneRepository.saveAll(milestones);
            logger.info("Milestones Saved!");

        } catch (IOException e){
            logger.error("Unable to save: " + e.getMessage());
        }
    }

}