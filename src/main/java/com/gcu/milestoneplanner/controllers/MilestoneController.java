package com.gcu.milestoneplanner.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gcu.milestoneplanner.domain.Milestone;
import com.gcu.milestoneplanner.domain.User;
import com.gcu.milestoneplanner.repository.MilestoneRepository;
import com.gcu.milestoneplanner.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
public class MilestoneController {

    private static final Logger logger = LoggerFactory.getLogger(MilestoneController.class);

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MilestoneRepository milestoneRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/generateMilestone")
    public void generateMileStone(
    @RequestParam("description") String description,
    @RequestParam("dueDate") String dueDate)     throws Exception{
        User user = getUserDetails();
        Milestone newMilestone = new Milestone();
        newMilestone.setDescription(description);
        newMilestone.setDueDate(dueDate);
        Milestone updatedMilestone = milestoneRepository.save(newMilestone);
        user.getMilestones().add(updatedMilestone.getId());
        userRepository.save(user);
        mapper.writeValue(new File("src\\main\\resources\\json\\users.json"), userRepository.findAll());
        mapper.writeValue(new File("src\\main\\resources\\json\\milestones.json"), milestoneRepository.findAll());
    }

    @GetMapping("/getMilestones")
    public List<Milestone> getMilestones() {
        User user = getUserDetails();
        Set<String> userMilestones = user.getMilestones();
        List<Milestone> milestones = new ArrayList<>();
        // Loop through users milestones, if available, add to response
        for (String id : userMilestones) {
            Optional<Milestone> milestone = milestoneRepository.findById(id);
            if (milestone.isPresent()) {
                milestones.add(milestone.get());
            } else {
                logger.error("Milestone with Id: " + id + " is missing");
            }
        }
        return milestones;
    }

    @GetMapping("/getSharedMilestones")
    public List<Milestone> getSharedMilestones() throws IOException {
        User user = getUserDetails();
        Set<String> userMilestones = user.getSharedMilestones();
        List<String> deletedMilestones = new ArrayList<>();
        List<Milestone> milestones = new ArrayList<>();
        // Loop through users shared milestones, if available, add to response, else remove milestone from list
        for (String id : userMilestones) {
            Optional<Milestone> milestone = milestoneRepository.findById(id);
            if (milestone.isPresent()) {
                milestones.add(milestone.get());
            } else {
                logger.error("Milestone with Id: " + id + " has been deleted, removing reference from shared list ...");
                deletedMilestones.add(id);
            }
        }
        userMilestones.removeAll(deletedMilestones);
        userRepository.save(user);
        mapper.writeValue(new File("src\\main\\resources\\json\\users.json"), userRepository.findAll());
        return milestones;
    }

    @PostMapping("/editMilestone")
    public void editMilestone(@RequestParam("id") String id,
                              @RequestParam("description") String description,
                              @RequestParam("dueDate") String dueDate,
                              @RequestParam("completionDate") String completionDate) throws Exception {
        User user = getUserDetails();
        if (user.getMilestones() != null && user.getMilestones().contains(id)) {
            Optional<Milestone> milestoneOptional = milestoneRepository.findById(id);
            if (milestoneOptional.isPresent()) {
                Milestone ms = milestoneOptional.get();
                ms.setDescription(description);
                ms.setDueDate(dueDate);
                ms.setCompletionDate(completionDate);
                milestoneRepository.save(ms);
                mapper.writeValue(new File("src\\main\\resources\\json\\milestones.json"), milestoneRepository.findAll());
            } else {
                logger.error("Milestone with Id: " + id + " is missing from db");
                //TODO pass response to UI alerting user save hasnt occurred
                throw new Exception();
            }
        }
    }

    @DeleteMapping("/removeMilestone")
    public void removeMilestone(@RequestParam("milestoneId") String milestoneId) throws IOException {
        //Remove from User
        User user = getUserDetails();
        user.getMilestones().remove(milestoneId);
        userRepository.save(user);
        mapper.writeValue(new File("src\\main\\resources\\json\\users.json"), userRepository.findAll());

        //Remove milestone from db
        Optional<Milestone> milestoneOptional = milestoneRepository.findById(milestoneId);
        if (milestoneOptional.isPresent()) {
            Milestone ms = milestoneOptional.get();
            milestoneRepository.delete(ms);
            logger.info("Milestones with Id: " + milestoneId + " deleted");
        } else {
            logger.error("Milestone with Id: " + milestoneId + " cannot be found");
        }
        mapper.writeValue(new File("src\\main\\resources\\json\\milestones.json"), milestoneRepository.findAll());
    }

    @PostMapping("/shareMilestones")
    public void shareMilestones(@RequestParam("milestones") List<String> milestones,
                                @RequestParam("users") List<String> users) throws IOException {
        for (String username : users) {
            Optional<User> userOptional = userRepository.findById(username);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                user.getSharedMilestones().addAll(milestones);
                userRepository.save(user);
                logger.info("Milestones with Ids: " + milestones +
                        "    successfully shared with : " + user.getUsername());
            } else {
                logger.error("User with username: " + username + " cannot be found");
            }
        }
        mapper.writeValue(new File("src\\main\\resources\\json\\users.json"), userRepository.findAll());
    }

    /*
     * Get Current User Details
     */
    public User getUserDetails() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
