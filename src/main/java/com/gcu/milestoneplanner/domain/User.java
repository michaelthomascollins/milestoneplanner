package com.gcu.milestoneplanner.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

@Document
public class User implements UserDetails {
    @Id
    private String username;
    private String password;
    private Set<String> milestones;
    private Set<String> sharedMilestones;
    private boolean enabled = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean accountNonExpired = true;
    private Collection<GrantedAuthority> authorities = null;

    public User() {
    }

    public User(String username, String password, Set<String> milestones, Set<String> sharedMilestones){
        this.username = username;
        this.password = password;
        this.milestones = milestones;
        this.sharedMilestones = sharedMilestones;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getMilestones() {
        return milestones;
    }

    public void setMilestones(Set<String> milestones) {
        this.milestones = milestones;
    }

    public Set<String> getSharedMilestones() {
        return sharedMilestones;
    }

    public void setSharedMilestones(Set<String> sharedMilestones) {
        this.sharedMilestones = sharedMilestones;
    }
}