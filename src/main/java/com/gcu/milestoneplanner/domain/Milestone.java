package com.gcu.milestoneplanner.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Milestone {

    @Id
    private String id;

    private String description;

    private String dueDate;

    private String completionDate;

    public Milestone() {
    }

    public Milestone(String id, String description, String dueDate, String completionDate) {
        this.id = id; 
        this.description = description;
        this.dueDate = dueDate;
        this.completionDate = completionDate;
    }

    public Milestone(String description, String dueDate, String completionDate) {
        this.description = description;
        this.dueDate = dueDate;
        this.completionDate = completionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getId() {
        return id;
    }

	public void setId(String id2) {
        this.id = id2;
	}

}
